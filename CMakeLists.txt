cmake_minimum_required(VERSION 3.10)

project(LAB1 CXX)

include_directories(core)
include_directories(graphics)

add_definitions(`wx-config --cxxflags`)
add_executable(test core/test.cpp core/ath.cpp core/Solution.cpp core/Graph.cpp)
add_executable(lab1 graphics/graph/chart.cpp graphics/graphicshelper/graphhelper.cpp graphics/App.cpp graphics/MainFrame.cpp core/Graph.cpp core/ath.cpp core/Solution.cpp)

target_link_libraries(lab1 -L/usr/lib/x86_64-linux-gnu -pthread   -lwx_gtk3u_xrc-3.0 -lwx_gtk3u_html-3.0 -lwx_gtk3u_qa-3.0 -lwx_gtk3u_adv-3.0 -lwx_gtk3u_core-3.0 -lwx_baseu_xml-3.0 -lwx_baseu_net-3.0 -lwx_baseu-3.0)