#pragma once

#include <cstddef>
#include <iostream>
#include <cstring>
#include <stdexcept>

using namespace std;

template<class T>
class DynamicArray{
private:
    T *data = nullptr;
    int arr_size = 0;
public:
    DynamicArray(int size){
        if (size < 0){
            throw runtime_error("invalid array size");
        }
        data = new T[size];
        memset(data, 0, size*sizeof(T));
        arr_size = size;
    }

    DynamicArray(T* items, int count){
        if (count < 0){
            throw runtime_error("invalid array size");
        }
        arr_size = count;
        data = new T[count];
        for(int i = 0; i < arr_size; i++){
            data[i] = items[i];
        }
    }

    DynamicArray(DynamicArray<T> const & dynamicArray){
        arr_size = dynamicArray.arr_size;
        data = new T[arr_size];
        for(int i = 0; i < arr_size; i++){
            data[i] = dynamicArray.data[i];
        }
    }

    ~DynamicArray(){
        delete [] data;
        arr_size = 0;
    }

    int &getData(int index) {
        if (index <= arr_size && index >= 0)
            return data[index]; 
        throw runtime_error("IndexOutOfRange");
    }

    T &operator[](int index){
        if (index < 0 || arr_size <= index){ 
            throw runtime_error("IndexOutOfRange");
        }

        return data[index];
    }

    int getSize(){
        return arr_size;
    }

    void setData(T value, int index){
        if (index < 0 || arr_size <= index){ 
            throw runtime_error("IndexOutOfRange");
        }
        data[index] = value;
    } 

    void del_last(){
        delete data[getSize() - 1];

    }

    void resize(int new_size){
        if (new_size < 0){
            throw runtime_error("invalid array size");
        }
        T* new_arr = new T[new_size];
        for(int i = 0; i < new_size; i++){
            new_arr[i] = data[i];
        }
        delete [] data;
        data = new_arr;
        arr_size = new_size;
    }

    DynamicArray<T> getSubArr(int startindex, int endindex){
        if (startindex < 0 || endindex >= getSize() || endindex < startindex) {
            throw runtime_error("IndexOutOfRange");
        }
        DynamicArray<T> res;
        for(int i = 0; i <= endindex; i++){
            if (i >= startindex){
                res.append(getData(i));
            }
        }
        return res;
    }

    DynamicArray<T> concat(DynamicArray<T> arr){
        DynamicArray<T> resArr;
        for (int i = 0; i < getSize(); i++){
            resArr.data[i] = this->data[i];
        }
        for (int i = getSize(); i < arr.getSize(); i++){
            resArr.data[i] = arr.data[i];
        }
        return resArr;
    }

    void print(){
        for(int i = 0; i < arr_size; i++){
            cout << data[i] << " ";
        }
        cout << "\n";
    }
};