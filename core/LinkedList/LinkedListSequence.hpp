#pragma once
#include <iostream>
#include "../Sequence.hpp"
#include "LinkedList.hpp"

using namespace std;

template <class T> 
class LinkedListSequence : public Sequence<T> {
private:
	LinkedList<T>* list;
public:
	LinkedListSequence() {
		list = new LinkedList<T>();
	}

	LinkedListSequence(int size) {
		list = new LinkedList<T>();
	}

	LinkedListSequence(T* items, int count) {
		list = new LinkedList<T>(items, count);
	}

	LinkedListSequence(LinkedList<T> const &list1){
		this->list = new LinkedList<T>(list1);
    }
    
	// LinkedListSequence(LinkedListSequence<T> && list){
	// 	this->list = list.list;
	// 	list.list = nullptr;
	// }

	LinkedListSequence(LinkedListSequence<T>  & list) : LinkedListSequence() {
		for (int i = 0; i < list.getLength(); i++)
			this->list->append(list.get(i));
	}

	~LinkedListSequence() {
		delete list;
	}

	int getLength() override{
		return list->getLength();
	}

	void append(T item) override {
		list->append(item);
	}

	void prepend(T item) override {
		list->prepend(item);
	}

	void delete_last(){
		list->del_last();
	}

	T &operator[](int index) {
		return get(index);
	}

	T getFirst() override {
		return list->getFirst();
	}

	T getLast() override {
		return list->getLast();
	}

	T &get(int index) override {
		return list->get(index);
	}

	void print() override {
		list->print();
	}

	void swap(int ind1, int ind2){
		list->swap(ind1, ind2);
	}

	void insertAt(T item, int index) override {
		list->insertAt(item, index);
	}

	LinkedListSequence<T> getSubsequence(int startIndex, int endIndex) {
		LinkedListSequence<T> tmpList;
		LinkedList<T> subList = list->getSubList(startIndex, endIndex);
		for (int i = 0; i < subList.getLength(); i++)
			tmpList.append(subList.get(i));
		return tmpList;
	}

	LinkedListSequence<T> operator+(LinkedListSequence<T> list1){
		LinkedListSequence<T> res(*(this->list) + *(list1.list));
        return res;
    }

	LinkedListSequence<T> operator=(LinkedListSequence<T> &&list1){
		this->list = list1.list;
		list1.list = nullptr;
		return *this;
	}

	LinkedListSequence<T> operator=(LinkedListSequence<T> const &list1){
		delete list;
		this->list = new LinkedList<T>(*list1.list);
		return *this;
	}


	LinkedListSequence<T> concat(LinkedListSequence<T> list) {
		LinkedList<T> tmpList;
		for (int i = 0; i < list.getLength(); i++)
			tmpList.append(list.get(i));
		LinkedList<T> concatedList = this->list->concat(tmpList);
		LinkedListSequence<T> resList;
		for (int i = 0; i < concatedList.getLength(); i++)
			resList.append(concatedList.get(i));
		return resList;
	}
};