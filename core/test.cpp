#include <algorithm>
#include <cstdlib>
#include <iostream>
#include <stdexcept>
#include <string>
#include <vector>
#include "Graph.h"
#include "Path.h"
#include "Solution.h"


bool ColorGraphTest(Graph graph){
    string graphColor = "The color assigned to vertex 0 is BLUE\n"
                        "The color assigned to vertex 1 is GREEN\n"
                        "The color assigned to vertex 2 is BLUE\n"
                        "The color assigned to vertex 3 is RED\n"
                        "The color assigned to vertex 4 is GREEN\n";

    string ans = graph.colorGraph();
    if(ans == graphColor){
        return true;
    } else {
        return false;
    }
}

bool TSPTest(Graph graph) {
    Solution s = Solution(graph);
    string calculed = s.calc({0,2}, 0).toString();
    string ans = "1->2->3->2->1 (12)";
    if(ans == calculed){
        return true;
    } else {
        return false;
    }
}

bool ShortestWayTest(Graph graph){
    vector<int> resBFS = graph.bfs(0, 4), res = {0, 1, 2, 4};
    if(resBFS == res){
        return true;
    } else {
        return false;
    }
}

bool FindOstovTest(Graph graph){
    vector<tuple<int,int,int>> vecOstov = graph.getOstov(), res;
    res.push_back(make_tuple(1, 0, 2));
    res.push_back(make_tuple(3, 0, 3));
    res.push_back(make_tuple(2, 1, 4));
    res.push_back(make_tuple(4, 2, 6));
    if(vecOstov == res){
        return true;
    } else{
        return false;
    }
}

void Tests(){
    Graph graph = Graph();
    std::cout << "Starting Tests\n\n";
    if (ColorGraphTest(graph)){
        cout << "ColorGraphTest passed\n";
    } else{
        cout << "ColorGraphTest failed\n";
    }
    if (TSPTest(graph)){
        cout << "TSPTest passed\n";
    } else{
        cout << "TSPTest failed\n";
    }
    if (ShortestWayTest(graph)){
        cout << "ShortestWayTest passed\n";
    } else{
        cout << "ShortestWayTest failed\n";
    }
    if (FindOstovTest(graph)){
        cout << "FindOstovTest passed\n";
    } else{
        cout << "FindOstovTest failed\n";
    }
}

int main(){
    Tests();
    return 0;
}
