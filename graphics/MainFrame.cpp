#include "MainFrame.h"
#include "wx/gdicmn.h"
#include "wx/gtk/colour.h"
#include "wx/wx/gdicmn.h"
#include "../graphics/graphicshelper/graphhelper.h"
#include <wx/listbook.h>
#include <wx/wx.h>


MainFrame::MainFrame(const wxString& title) : wxFrame(nullptr, wxID_ANY, title) {
	panel = new wxPanel(this);
	panel->SetBackgroundColour(wxColour(30, 30, 30));

	topicTxtStat = new wxStaticText(panel, wxID_ANY, "Graph", wxPoint(100, 20), wxSize(10,10));
	topicTxtStat->SetForegroundColour(*wxWHITE);
	wxFont font = topicTxtStat->GetFont();
	font.SetPointSize(12);
	font.SetWeight(wxFONTWEIGHT_BOLD);
	topicTxtStat->SetFont(font);

	topsCountTxtStat = new wxStaticText(panel, wxID_ANY, "Enter graph tops: ", wxPoint(30, 60), wxDefaultSize);
	topsCountTxtStat->SetForegroundColour(*wxWHITE);
	topsCountTxtCtrl = new wxTextCtrl(panel, wxID_ANY, "", wxPoint(140, 55), wxSize(80, -1));
	topsCountTxtCtrl->SetBackgroundColour(wxColour(50,50,50));
	topsCountTxtCtrl->SetOwnForegroundColour(wxColour(255,255,255));

	arcsCountTxtStat = new wxStaticText(panel, wxID_ANY, "Enter graph arcs: ", wxPoint(30, 90), wxDefaultSize);
	arcsCountTxtStat->SetForegroundColour(*wxWHITE);
	arcsCountTxtCtrl = new wxTextCtrl(panel, wxID_ANY, "", wxPoint(140, 85), wxSize(80, -1));
	arcsCountTxtCtrl->SetBackgroundColour(wxColour(50,50,50));
	arcsCountTxtCtrl->SetOwnForegroundColour(wxColour(255,255,255));

	cteateGraphBtn = new wxButton(panel, wxID_ANY, "create Graph", wxPoint(30, 115), wxSize(100,30));
	cteateGraphBtn->SetBackgroundColour(wxColor(50, 50, 50));
	cteateGraphBtn->SetOwnForegroundColour(wxColour(255,255,255));

	createOrGraphBtn = new wxButton(panel, wxID_ANY, "create OrGraph", wxPoint(140, 115), wxSize(100,30));
	createOrGraphBtn->SetBackgroundColour(wxColor(50, 50, 50));
	createOrGraphBtn->SetOwnForegroundColour(wxColour(255,255,255));
	line1 = new wxStaticLine(panel, wxID_ANY, wxPoint(10, 150), wxSize(235, 5));

	//

	colorGraphTxtStat = new wxStaticText(panel, wxID_ANY, "Color Graph ", wxPoint(80, 170), wxSize(10,10));
	colorGraphTxtStat->SetFont(font);
	colorGraphTxtStat->SetForegroundColour(*wxWHITE);

	colorGraphBtn = new wxButton(panel, wxID_ANY, "color Graph", wxPoint(30, 200), wxDefaultSize);
	colorGraphBtn->SetBackgroundColour(wxColor(50, 50, 50));
	colorGraphBtn->SetOwnForegroundColour(wxColour(255,255,255));

	line2 = new wxStaticLine(panel, wxID_ANY, wxPoint(10, 240), wxSize(235, 5));

	//

	tspTxtStat = new wxStaticText(panel, wxID_ANY, "TSP", wxPoint(105, 260), wxSize(10,10));
	tspTxtStat->SetFont(font);
	tspTxtStat->SetForegroundColour(*wxWHITE);

	startTopTxtStat = new wxStaticText(panel, wxID_ANY, "Enter top to start with: ", wxPoint(30, 300), wxDefaultSize);
	startTopTxtStat->SetForegroundColour(*wxWHITE);
	startTopTxtCtrl = new wxTextCtrl(panel, wxID_ANY, "", wxPoint(170, 295), wxSize(30, -1));
	startTopTxtCtrl->SetBackgroundColour(wxColour(50,50,50));
	startTopTxtCtrl->SetOwnForegroundColour(wxColour(255,255,255));

	visitTopTxtStat = new wxStaticText(panel, wxID_ANY, "Enter tops to visit: ", wxPoint(30, 330), wxDefaultSize);
	visitTopTxtStat->SetForegroundColour(*wxWHITE);
	visitTopTxtCtrl = new wxTextCtrl(panel, wxID_ANY, "", wxPoint(145, 325), wxSize(80, -1));
	visitTopTxtCtrl->SetBackgroundColour(wxColour(50,50,50));
	visitTopTxtCtrl->SetOwnForegroundColour(wxColour(255,255,255));

	processTspBtn = new wxButton(panel, wxID_ANY, "process", wxPoint(30, 355), wxDefaultSize);
	processTspBtn->SetBackgroundColour(wxColor(50, 50, 50));
	processTspBtn->SetOwnForegroundColour(wxColour(255,255,255));

	line3 = new wxStaticLine(panel, wxID_ANY, wxPoint(10, 390), wxSize(235, 5));

	//

	findShorWayTxtStat = new wxStaticText(panel, wxID_ANY, "Find The Shortest Way", wxPoint(50, 410), wxSize(10,10));
	findShorWayTxtStat->SetFont(font);
	findShorWayTxtStat->SetForegroundColour(*wxWHITE);

	firstTopTxtStat = new wxStaticText(panel, wxID_ANY, "Enter top to start with: ", wxPoint(30, 450), wxDefaultSize);
	firstTopTxtStat->SetForegroundColour(*wxWHITE);
	firstTopTxtCtrl = new wxTextCtrl(panel, wxID_ANY, "", wxPoint(168, 445), wxSize(30, -1));
	firstTopTxtCtrl->SetBackgroundColour(wxColour(50,50,50));
	firstTopTxtCtrl->SetOwnForegroundColour(wxColour(255,255,255));
	
	secondTopTxtStat = new wxStaticText(panel, wxID_ANY, "Enter top to end with: ", wxPoint(30, 480), wxDefaultSize);
	secondTopTxtStat->SetForegroundColour(*wxWHITE);
	secondTopTxtCtrl = new wxTextCtrl(panel, wxID_ANY, "", wxPoint(168, 475), wxSize(30, -1));
	secondTopTxtCtrl->SetBackgroundColour(wxColour(50,50,50));
	secondTopTxtCtrl->SetOwnForegroundColour(wxColour(255,255,255));

	processShortestWayBtn = new wxButton(panel, wxID_ANY, "Find", wxPoint(30, 503), wxDefaultSize);
	processShortestWayBtn->SetBackgroundColour(wxColor(50, 50, 50));
	processShortestWayBtn->SetOwnForegroundColour(wxColour(255,255,255));

	line4 = new wxStaticLine(panel, wxID_ANY, wxPoint(10, 540), wxSize(235, 5));

	//
	backboneTxtStat = new wxStaticText(panel, wxID_ANY, "Find Ostov ", wxPoint(85, 550), wxSize(10,10));
	backboneTxtStat->SetFont(font);
	backboneTxtStat->SetForegroundColour(*wxWHITE);

	backboneBtn = new wxButton(panel, wxID_ANY, "Find", wxPoint(30, 568), wxSize(60,27));
	backboneBtn->SetBackgroundColour(wxColor(50, 50, 50));
	backboneBtn->SetOwnForegroundColour(wxColour(255,255,255));

	line5 = new wxStaticLine(panel, wxID_ANY, wxPoint(250, 10), wxSize(5, 580));

	// 

	txtPrint = new wxTextCtrl(panel, wxID_ANY, "", wxPoint(270, 20), wxSize(520, 500), wxTE_MULTILINE);
	txtPrint->SetBackgroundColour(wxColor(50, 50, 50));
	txtPrint->SetOwnForegroundColour(wxColour(255,255,255));

	clearBtn = new wxButton(panel, wxID_ANY, "Clear", wxPoint(710, 525), wxSize(80, -1));
	clearBtn->SetBackgroundColour(wxColor(50, 50, 50));
	clearBtn->SetOwnForegroundColour(wxColour(255,255,255));

	// testsBtn = new wxButton(panel, wxID_ANY, "show tests", wxPoint(270, 525), wxDefaultSize);
	// showChartsBtn = new wxButton(panel, wxID_ANY, "show chart", wxPoint(360, 525), wxDefaultSize);
	// hideChartsBtn = new wxButton(panel, wxID_ANY, "hide chart", wxPoint(360, 560), wxDefaultSize);

	//Bindings
	cteateGraphBtn->Bind(wxEVT_BUTTON, &MainFrame::OnCreateGraphBtnClicked, this);
	createOrGraphBtn->Bind(wxEVT_BUTTON, &MainFrame::OnCreateOrGraphBtnClicked, this);
	colorGraphBtn->Bind(wxEVT_BUTTON, &MainFrame::OnColorGraphBtnClicked, this);
	processTspBtn->Bind(wxEVT_BUTTON, &MainFrame::OnProcessTspBtnClicked, this);
	processShortestWayBtn->Bind(wxEVT_BUTTON, &MainFrame::OnProcessShortestWayBtnClicked, this);
	backboneBtn->Bind(wxEVT_BUTTON, &MainFrame::OnBackboneBtnBtnClicked, this);
	clearBtn->Bind(wxEVT_BUTTON, &MainFrame::OnClearGraphBtnClicked, this);
	// showChartsBtn->Bind(wxEVT_BUTTON, &MainFrame::OnShowChartsBtnClicked, this);
	// hideChartsBtn->Bind(wxEVT_BUTTON, &MainFrame::OnHideChartsBtnClicked, this);
}

void MainFrame::OnCreateGraphBtnClicked(wxCommandEvent& evt) {
	int tops = stoi(topsCountTxtCtrl->GetValue().ToStdString());
	int arcs = stoi(arcsCountTxtCtrl->GetValue().ToStdString());
	presenter.createGraph(tops, arcs);

	txtPrint->WriteText("Graph: \n" + presenter.getGraph() + "\n\n");
}

void MainFrame::OnCreateOrGraphBtnClicked(wxCommandEvent& evt) {
	int tops = stoi(topsCountTxtCtrl->GetValue().ToStdString());
	int arcs = stoi(arcsCountTxtCtrl->GetValue().ToStdString());
	presenter.createOrGraph(tops, arcs, 1);

	txtPrint->WriteText("Graph: \n" + presenter.getGraph() + "\n\n");
}

void MainFrame::OnColorGraphBtnClicked(wxCommandEvent& evt) {
	pair<string, string> pair = presenter.colorGraph();

	txtPrint->WriteText("Colored graph: \n" + pair.first + "\n");
	txtPrint->WriteText("Time to color: " + pair.second + "\n\n");
}

void MainFrame::OnProcessTspBtnClicked(wxCommandEvent& evt) {
	int begin = stoi(startTopTxtCtrl->GetValue().ToStdString());
	string vesitTops = visitTopTxtCtrl->GetValue().ToStdString();
	
	pair<string, string> pair = presenter.tspGraph(vesitTops, begin);

	txtPrint->WriteText("TSP solution: \n" + pair.first + "\n");
	txtPrint->WriteText("Time of processing: " + pair.second + "\n\n");
}

void MainFrame::OnProcessShortestWayBtnClicked(wxCommandEvent& evt) {
	int begin = stoi(firstTopTxtCtrl->GetValue().ToStdString());
	int end = stoi(secondTopTxtCtrl->GetValue().ToStdString());

	pair<string, string> pair = presenter.shortestWay(begin, end);

	txtPrint->WriteText("The shortest way is: \n" + pair.first + "\n");
	txtPrint->WriteText("Time of processing: " + pair.second + "\n\n");
}

void MainFrame::OnBackboneBtnBtnClicked(wxCommandEvent& evt) {
	pair<string, string> pair = presenter.backBone();
	txtPrint->WriteText("Ostov: \n" + pair.first + "\n");
	txtPrint->WriteText("Time of processing: " + pair.second + "\n\n");
}

void MainFrame::OnClearGraphBtnClicked(wxCommandEvent& evt) {
	txtPrint->Clear();
}

void MainFrame::OnShowChartsBtnClicked(wxCommandEvent& evt) {

}

void MainFrame::OnHideChartsBtnClicked(wxCommandEvent& evt) {

}