#pragma once
#include "vector"

#include "../../core/DynamicArray/ArraySequence.hpp"
#include "../../core/LinkedList/LinkedListSequence.hpp"
#include "../../core/Graph.h"
#include "../../core/Path.h"
#include "../../core/Solution.h"

#include <tuple>
#include <vector>
#include <time.h>

class Presenter {
private:
	Graph* graph;
public:
	void createGraph(int v, int r);
	void createOrGraph(int v, int r, bool orient);
	string getGraph();
	pair<string, string> colorGraph(); // <colored graph, time to color>
	pair<string, string> tspGraph(string way, int begin); // <tsp way, time to color>
	pair<string, string> shortestWay(int begin, int end);
	pair<string, string> backBone(); // <backBone, time>
};